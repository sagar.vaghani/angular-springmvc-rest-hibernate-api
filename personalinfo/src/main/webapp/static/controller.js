var app=angular.module('myModule',['ui.bootstrap']);


app.controller("dataCtrl",function($scope, $http){
	
		  
$scope.user={};
$scope.persons=[];
$scope.isViewReady=false;
	$scope.create=function(){
		console.log($scope.user);
		$http.post('/personalinfo/person/create', $scope.user)
        .then(
        function (response) {
            console.log(response.data);
            $scope.view();
        },
        function(errResponse){
            console.error('Error while creating Person');
            console.error(errResponse);
        });
	}
	$scope.view=function(){
		
				$http.get('/personalinfo/person/view')
		        .then(
		        function (response) {
		            $scope.persons=(response.data);
		            $scope.isViewReady=true;
		        },
		        function(errResponse){
		            console.error('Error while fetching Person list');
		            console.error(errResponse);
		        });
			}
});