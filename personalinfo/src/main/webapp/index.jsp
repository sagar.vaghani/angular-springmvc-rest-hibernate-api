<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Personal Info App</title>  
     <link href="<c:url value='/static/bootstrap.min.css'/>" rel="stylesheet">
  </head>
  <body ng-app="myModule" class="ng-cloak">
      <div class="container" ng-controller="dataCtrl">
          <div class="panel panel-info" style="margin-top:20px;">
              <div class="panel-heading"><span class="lead">Person Information Form </span></div>
              <div class="formcontainer">
                
                      <div class="row" style="margin-top:10px;">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="uname">Name</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="user.username" id="uname" class="username form-control input-sm" placeholder="Enter your name" />                                 
                              </div>
                          </div>
                      </div>
                         
                       
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="address">Address</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="user.address" id="address" class="form-control input-sm" placeholder="Enter your Address"/>
                              </div>
                          </div>
                      </div>
 
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="email">Email</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="user.email" id="email" class="email form-control input-sm" placeholder="Enter your Email"/>
                              </div>
                          </div>
                      </div>
 
                      <div class="row">
                          <div class="form-actions ">
                              <button type="button" ng-click="create()" style="margin-left:30px;" class="btn btn-primary">Create</button>
                          </div>
                      </div>
                  
              </div>
          </div>
          <hr/>
          <button type="button" ng-click="view()"  class="btn btn-success">View List</button>
          <hr/>
           <div class="panel panel-success" ng-if="isViewReady">
             
              <div class="panel-heading"><span class="lead">List of Existing Persons </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Address</th>
                              <th>Email</th>                              
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in persons">
                              <td><span ng-bind="u.id"></span></td>
                              <td><span ng-bind="u.username"></span></td>
                              <td><span ng-bind="u.address"></span></td>
                              <td><span ng-bind="u.email"></span></td>
                           </tr>
                      </tbody>
                  </table>
              </div>
          </div> 
      </div>
      
   
      <script src="<c:url value='/static/angular.js' />"></script>
      <script src="<c:url value='/static/jquery.min.js' />"></script>
      <script src="<c:url value='/static/bootstrap.min.js' />"></script>
      <script src="<c:url value='/static/ui-bootstrap-tpls-2.5.0.js' />"></script>
      <script src="<c:url value='/static/controller.js' />"></script>
  </body>
</html>

