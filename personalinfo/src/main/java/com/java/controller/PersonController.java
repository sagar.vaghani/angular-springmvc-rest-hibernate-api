package com.java.controller;
  
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.java.model.Person;
import com.java.service.PersonService;
  
@RestController
public class PersonController {
  
    @Autowired
    PersonService personService; 
 
    @RequestMapping(value = "/person/create", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody Person user,    UriComponentsBuilder ucBuilder) {
  
        personService.saveUser(user);
  
        HttpHeaders headers = new HttpHeaders();
        
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
  
     
    @RequestMapping(value = "/person/view", method = RequestMethod.GET)
    public ResponseEntity<List<Person>> listAllUsers() {
        List<Person> users = personService.findAllPersons();
        if(users.isEmpty()){
            return new ResponseEntity<List<Person>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Person>>(users, HttpStatus.OK);
    }
   
  
}