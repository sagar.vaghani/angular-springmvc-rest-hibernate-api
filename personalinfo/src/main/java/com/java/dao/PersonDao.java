package com.java.dao;

import java.util.List;

import com.java.model.Person;

public interface PersonDao {
		 
	    void savePerson(Person p);

		List<Person> fetchPersonList();
	     
	   
	}

