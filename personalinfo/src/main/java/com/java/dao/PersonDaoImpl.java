package com.java.dao;



import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.java.model.Person;

@Repository("personDao")
@org.springframework.transaction.annotation.Transactional
public class PersonDaoImpl extends AbstractDao implements PersonDao{
 
    public void savePerson(Person p) {
        persist(p);
    }

	@Override
	public List<Person> fetchPersonList() {
		// TODO Auto-generated method stub
		 Criteria criteria = getSession().createCriteria(Person.class);
	        return (List<Person>) criteria.list();
	}
 
   
     
}
