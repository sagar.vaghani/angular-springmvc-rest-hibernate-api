package com.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PERSON")
public class Person {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	 @Column(name = "NAME", nullable = true	)
    private String username;
	 @Column(name = "ADDRESS", nullable = true)
    private String address;
	 @Column(name = "EMAIL", nullable = true)
    private String email;
     
    public Person(){
        
    }
     
    public Person(long id, String username, String address, String email){
        this.id = id;
        this.username = username;
        this.address = address;
        this.email = email;
    }
 
    public long getId() {
        return id;
    }
 
    public void setId(long id) {
        this.id = id;
    }
 
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getAddress() {
        return address;
    }
 
    public void setAddress(String address) {
        this.address = address;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Person))
            return false;
        Person other = (Person) obj;
        if (id != other.id)
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return "Person [id=" + id + ", username=" + username + ", address=" + address
                + ", email=" + email + "]";
    }
     
 
     
}