package com.java.service;

import java.util.List;

import com.java.model.Person;

public interface PersonService {
	public void saveUser(Person user);

	public List<Person> findAllPersons();
}
