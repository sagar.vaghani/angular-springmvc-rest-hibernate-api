package com.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.dao.PersonDao;
import com.java.model.Person;

@Service("personService")
public class PersonServiceImpl implements PersonService{
     
	@Autowired
    private PersonDao dao;
     
    public void saveUser(Person user) {
    	dao.savePerson(user);
      
    }

	@Override
	public List<Person> findAllPersons() {
		return dao.fetchPersonList();
	}
 
   
 
}
